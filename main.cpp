#include <SFML/Graphics.hpp>
#include <iostream>
#include <vector>
#include <cmath>
#include "Tree.h"



class GeometryObject {
public:
    virtual bool isInsideFrustum(const sf::ConvexShape& frustum) const = 0;
    virtual bool isTouchingFrustum(const sf::ConvexShape& frustum) const = 0;
    virtual void draw(sf::RenderWindow& window, bool insideFrustum, bool touchingFrustum) = 0;
    virtual ~GeometryObject() {}
};

class Square : public GeometryObject {
public:
    Square(float x, float y) : position(x, y) {
        square.setPosition(position);
        square.setSize(sf::Vector2f(50, 50));
    }

    bool isInsideFrustum(const sf::ConvexShape& frustum) const override {
        return frustum.getGlobalBounds().intersects(square.getGlobalBounds());
    }

    bool isTouchingFrustum(const sf::ConvexShape& frustum) const override {
        auto bounds = square.getGlobalBounds();
        return frustum.getGlobalBounds().intersects(bounds) &&
            (bounds.left <= frustum.getGlobalBounds().left + frustum.getGlobalBounds().width) &&
            (bounds.left + bounds.width >= frustum.getGlobalBounds().left) &&
            (bounds.top <= frustum.getGlobalBounds().top + frustum.getGlobalBounds().height) &&
            (bounds.top + bounds.height >= frustum.getGlobalBounds().top);
    }

    void draw(sf::RenderWindow& window, bool insideFrustum, bool touchingFrustum) override {
        if (touchingFrustum && !insideFrustum) {
            square.setFillColor(sf::Color::Blue);
        }
        else {
            square.setFillColor(insideFrustum ? sf::Color::Green : sf::Color::Red);
        }
        window.draw(square);
    }

private:
    sf::RectangleShape square;
    sf::Vector2f position;
};

class Circle : public GeometryObject {
public:
    Circle(float x, float y) : position(x, y) {
        circle.setPosition(position);
        circle.setRadius(25);
    }

    bool isInsideFrustum(const sf::ConvexShape& frustum) const override {
        return frustum.getGlobalBounds().intersects(circle.getGlobalBounds());
    }

    bool isTouchingFrustum(const sf::ConvexShape& frustum) const override {
        return frustum.getGlobalBounds().intersects(circle.getGlobalBounds());
    }

    void draw(sf::RenderWindow& window, bool insideFrustum, bool touchingFrustum) override {
        if (touchingFrustum && !insideFrustum) {
            circle.setFillColor(sf::Color::Blue);
        }
        else {
            circle.setFillColor(insideFrustum ? sf::Color::Green : sf::Color::Red);
        }
        window.draw(circle);
    }

private:
    sf::CircleShape circle;
    sf::Vector2f position;
};

class Triangle : public GeometryObject {
public:
    Triangle(float x, float y) {
        vertices[0] = sf::Vector2f(x, y);
        vertices[1] = sf::Vector2f(x + 50, y);
        vertices[2] = sf::Vector2f(x + 25, y - 50);

        triangle.setPointCount(3);
        triangle.setPoint(0, vertices[0]);
        triangle.setPoint(1, vertices[1]);
        triangle.setPoint(2, vertices[2]);
    }

    bool isInsideFrustum(const sf::ConvexShape& frustum) const override {
        for (const auto& vertex : vertices) {
            if (frustum.getGlobalBounds().contains(vertex)) {
                return true;
            }
        }
        return false;
    }

    bool isTouchingFrustum(const sf::ConvexShape& frustum) const override {
        for (const auto& vertex : vertices) {
            if (frustum.getGlobalBounds().intersects(sf::FloatRect(vertex, sf::Vector2f(1, 1)))) {
                return true;
            }
        }
        return false;
    }

    void draw(sf::RenderWindow& window, bool insideFrustum, bool touchingFrustum) override {
        if (touchingFrustum && !insideFrustum) {
            triangle.setFillColor(sf::Color::Blue);
        }
        else {
            triangle.setFillColor(insideFrustum ? sf::Color::Green : sf::Color::Red);
        }
        window.draw(triangle);
    }

private:
    sf::ConvexShape triangle = sf::ConvexShape(3);
    sf::Vector2f vertices[3];
};



int main() {
    sf::RenderWindow window(sf::VideoMode(1000, 800), "Frustum Culling SFML");
    window.setFramerateLimit(60);

    Tree tree;

    Square square1(100, 100);
    Square square2(250, 250);
    Circle circle1(400, 240);
    Circle circle2(550, 400);
    Triangle triangle1(50, 400);
    Triangle triangle2(700, 100);
    Triangle triangle3(300, 550);
     
    tree.addObject(&square1);
    tree.addObject(&square2);
    tree.addObject(&circle1);
    tree.addObject(&circle2);
    tree.addObject(&triangle1);
    tree.addObject(&triangle2);
    tree.addObject(&triangle3);

    sf::ConvexShape frustumShape(4);
    frustumShape.setPoint(0, sf::Vector2f(200, 200));
    frustumShape.setPoint(1, sf::Vector2f(600, 200));
    frustumShape.setPoint(2, sf::Vector2f(800, 600));
    frustumShape.setPoint(3, sf::Vector2f(200, 600));
    frustumShape.setOutlineColor(sf::Color::Red);
    frustumShape.setOutlineThickness(2);
    frustumShape.setFillColor(sf::Color::Transparent);

    while (window.isOpen()) {
        sf::Event event;
        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        window.clear(sf::Color::White);

        window.draw(frustumShape); 

        auto objectsInsideFrustum = tree.objectsInsideFrustum(frustumShape);
        for (const auto& object : objectsInsideFrustum) {
            object->draw(window, true, object->isTouchingFrustum(frustumShape));
        }

        const auto& allObjects = tree.getObjects();
        for (const auto& object : allObjects) {
            if (std::find(objectsInsideFrustum.begin(), objectsInsideFrustum.end(), object) == objectsInsideFrustum.end()) {
                object->draw(window, false, object->isTouchingFrustum(frustumShape));
            }
        }

        window.display();
    }

    return 0;
}